/*
 * ChainedHashMap.cpp
 *
 *  Created on: 23 Oct 2014
 *      Author: jvullo
 */

#include "ChainedHashDictionary.h"
#include "ChainedHashWord.h"

#include <stdio.h>
#include <string.h>
#include <sstream>
#include <utility>
#include <locale>
#include <iostream>
#include <vector>

using namespace std;

namespace SpellCheckerProgram
{


	const long MAX_TABLE_SIZE = 88405; //Max size of the dictionary with 10% extra

	ChainedHashDictionary::ChainedHashDictionary() {
		dictionary = new ChainedHashWord*[MAX_TABLE_SIZE];
		for (int i = 0; i < MAX_TABLE_SIZE; i++)
			dictionary[i] = NULL;
	}

	/**
	 * Exactly like get word, but instead returns the number of hops it had to take to get the outcome.
	 */
	int ChainedHashDictionary::getWordHops(string key) {

		//Some magic to turn everything searched for to lower case.
		string lowerCaseKey;
	    std::locale loc;
	    for (unsigned int i = 0; i < key.length(); ++i)
	    {
	    	lowerCaseKey += std::tolower(key.at(i), loc);
	    }

	    key = lowerCaseKey;

	    int hash = hashFunction(key);

	    int hopCount = 0;
		if (dictionary[hash] == NULL) {
			return -1;
		} else {
			ChainedHashWord *tableEntry = dictionary[hash];
			while (tableEntry != NULL && (tableEntry->getWordKey().compare(key) != 0))
			{
				tableEntry = tableEntry->getNextEntry();
				hopCount++;
			}

			if (tableEntry != NULL)
			{
				return hopCount + 1;
			}
			else
			{
				return 0;
			}
		}
	}

	/**
		 * Retrieves the word from the table entries. Ignores case.
		 *  Returns an empty string if the variable is empty.
		 */
		string ChainedHashDictionary::getWord(string key, int &pointed) {

			//Some magic to turn everything searched for to lower case
			string lowerCaseKey;
		    std::locale loc;
		    for (unsigned int i = 0; i < key.length(); ++i)
		    {
		    	lowerCaseKey += std::tolower(key.at(i), loc);
		    }

		    key = lowerCaseKey;

		    int hash = hashFunction(key);

		    pointed = 0;

			if (dictionary[hash] == NULL) {
				pointed = 1;
				return "";
			} else {
				ChainedHashWord *tableEntry = dictionary[hash];
				while (tableEntry != NULL && (tableEntry->getWordKey().compare(key) != 0))
				{
					tableEntry = tableEntry->getNextEntry();
					pointed++;
					//cout << hopCount << " -- ";
				}

				if (tableEntry != NULL)
				{
					//If pointed is equal to 0, then return 1 since the looping ^^ has never run.
					if (pointed == 0) {
						pointed = 1;
					}
					return tableEntry->getWordValue();
				}
				else
				{
					pointed = 1;
					return "";
				}
			}

		}

	/**
	 * Retrieves the word from the table entries. Ignores case.
	 *  Returns an empty string if the variable is empty.
	 */
	string ChainedHashDictionary::getWord(string key) {

		//Some magic to turn everything searched for to lower case
		string lowerCaseKey;
	    std::locale loc;
	    for (unsigned int i = 0; i < key.length(); ++i)
	    {
	    	lowerCaseKey += std::tolower(key.at(i), loc);
	    }

	    key = lowerCaseKey;

	    int hash = hashFunction(key);

		if (dictionary[hash] == NULL) {
			return "";
		} else {
			ChainedHashWord *tableEntry = dictionary[hash];
			while (tableEntry != NULL && (tableEntry->getWordKey().compare(key) != 0))
			{
				tableEntry = tableEntry->getNextEntry();
			}

			if (tableEntry != NULL)
			{
				return tableEntry->getWordValue();
			}
			else
			{
				return "";
			}
		}
	}



	/**
	 * Takes just the value and puts it into the hash table.
	 */
	void ChainedHashDictionary::putWord(string value) {

		string key = value;

		int hash = hashFunction(key);

		if (dictionary[hash] != NULL) {
			ChainedHashWord *entry = dictionary[hash];
			//While looks for the next null entry.

			while (entry->getNextEntry() != NULL)
			{
				entry = entry->getNextEntry();
			}

			if (entry->getWordKey().compare(key) != 0)
			{
				entry->setNextWord(new ChainedHashWord(value));
			}
			else
			{
				entry->setWordValue(value);
			}
		}
		else {
			dictionary[hash] = new ChainedHashWord(value);
		}
	}

	float ChainedHashDictionary::calculateLoadFactor() {

		float countOfEntries = 0;

		for (int i = 0; i < MAX_TABLE_SIZE; i++) {
			if (dictionary[i] != NULL) {
				countOfEntries++;

			}
		}

		cout << "Number of entries: = " << countOfEntries << " table Size is: " << MAX_TABLE_SIZE <<
				" therefore the load factor is: " << countOfEntries/MAX_TABLE_SIZE << endl;

		return countOfEntries/MAX_TABLE_SIZE;

	}

	/**
	 * Prints the table.
	 */
	void ChainedHashDictionary::printHashTable() {

		cout << endl;
		//For the length of the table, search for elements that aren't null and print them.
		for (int i = 0; i < MAX_TABLE_SIZE; i++) {
			if (dictionary[i] != NULL) {
				cout << "Dictionary[" << i << "]";
				ChainedHashWord *entry = dictionary[i];
				//cout << "Hash value = " << hashFunction(entry->getKey()) << endl;
				cout << " " << entry->getWordValue();

				while (entry->getNextEntry() != NULL) {
					entry = entry->getNextEntry();
					cout << " -- " << entry->getWordValue();
				}
				cout << endl;
			}
		}
	}

	/**
	 * Wrapper that calls the other hash functions. This is done for easy swapping during testing.
	 */
	int ChainedHashDictionary::hashFunction (string newItem) {
		int a = djb2HashFunction(newItem);
		return a;
	}

	/**
	 * A very poor hash function that should yield a very high collision.
	 *
	 * Just simply uses size. I wrote this one myself.
	 */
	int ChainedHashDictionary::abysmalHashFunction (string newItem) {
		return (newItem.size() % MAX_TABLE_SIZE);
	}

	/**
	 * A poor hash function that should yield a high collision.
	 *
	 * Somes up the value of the ascii, I wrote this one myself inspired by Chris Cox's lectures.
     */
	int ChainedHashDictionary::badHashFunction (string newItem) {

		int ascii = 0;

		 for (unsigned int i = 0; i < newItem.length(); i++)
		    {
		        char x = newItem.at(i);
		        ascii = int(x);
		    }

		 return (ascii % MAX_TABLE_SIZE);
	}

	/**
	 * A good hash function that should yield a good collision.
	 * This algorithm by Dan Bernstein
	 */
	int ChainedHashDictionary::djb2HashFunction (string key) {

		long hash = 5381;
		int c;
		unsigned char* uc = (unsigned char*)key.c_str();
		while (c = *uc++) { // DJB2
			hash = ((hash << 5) + hash) + c;
		}
		return abs(hash % MAX_TABLE_SIZE);
	}

	/**
	 * Borrowed this hash function from here http://programmingnotes.freeweq.com/?p=2999
	 * It seems to work the best.
	 */
	int ChainedHashDictionary::oldHashFunction (string newItem) {
		long h = 19937;

		string temp = newItem;

		for(unsigned x=0; x < temp.length(); ++x)
		{
			h = (h << 6) ^ (h >> 26) ^ temp[x];
		}

		return abs(h % MAX_TABLE_SIZE);
	}

	/**
	 * Calculates the number of hash entries that do not have any values.
	 */
	void ChainedHashDictionary::calculateNoEmptyEntries() {

		int countOfEmptyEntries = 0;

		for (int i = 0; i < MAX_TABLE_SIZE; i++) {
			if (dictionary[i] == NULL) {
				countOfEmptyEntries++;
			}
		}

		cout << "The count of the empty entries was: " << countOfEmptyEntries << endl;

	}

	/**
	 * Prints the number of the largest chain.
	 */
	void ChainedHashDictionary::calculateLargestChain() {

		int currentMax = 0;
		int chainLengthIndex = 0;
		string hash = "";

		for (int i = 0; i < MAX_TABLE_SIZE; i++) {
			if (dictionary[i] != NULL) {
				ChainedHashWord *entry = dictionary[i];
				while (entry->getNextEntry() != NULL) {
					entry = entry->getNextEntry();
					chainLengthIndex ++;
				}

				if (chainLengthIndex > currentMax) {
					currentMax = chainLengthIndex;
					//hash = hashFunction(entry->getWordValue());
				} else if (chainLengthIndex == currentMax) {
					ostringstream convert;
					convert << hashFunction(entry->getWordValue());
					hash.append(" " +  convert.str());
				}

				chainLengthIndex = 0;
			}
		}
										//	+1 because its 0 indexed.
		cout << "Current longest chain is " << (currentMax + 1) << " dictionary hash entries:" << hash << endl;
	}

}

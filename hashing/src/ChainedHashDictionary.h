/*
 * ChainedChainedHashMap.h
 *
 *  Created on: 22 Oct 2014
 *      Author: jvullo
 */

#ifndef _CHAINEDHASHMAP_
#define _CHAINEDHASHMAP_

#include "ChainedHashWord.h"
#include "HashWord.h"

#include <stdio.h>
#include <string.h>
#include <sstream>
#include <utility>
#include <locale>

using namespace std;

namespace SpellCheckerProgram
{

	class ChainedHashDictionary {
		private:
			ChainedHashWord **dictionary;
			int badHashFunction (string newItem);
			int abysmalHashFunction (string newItem);
			int djb2HashFunction (string newItem);
			int oldHashFunction (string newItem);

		public:
			ChainedHashDictionary();
			string getWord(string key, int &pointed);
			string getWord(string key);
			void putWord(string value);
			float calculateLoadFactor();
			void deleteTable();
			void printHashTable();
			int getWordHops(string);
			void calculateNoEmptyEntries();
			void calculateLargestChain();
			int hashFunction (string newItem);
	};
}

#endif /* CHAINEDHASHMAP_H_ */

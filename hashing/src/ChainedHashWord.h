/*
 * ChainedHashEntry.h
 *
 * Extends the HashEntry class further to implement a chaining version of hashing.
 *
 *  Created on: 22 Oct 2014
 *      Author: jvullo
 */


#ifndef _CHAINEDHASHENTRY_
#define _CHAINEDHASHENTRY_

#include "HashWord.h"
#include <cstdlib>

using namespace std;

namespace SpellCheckerProgram
{

	class ChainedHashWord : public HashWord
	{

		private:
			ChainedHashWord *nextWord;

		public:
			ChainedHashWord(string value) : HashWord(value) {
				this->nextWord = NULL;
			}

			ChainedHashWord *getNextEntry() {
				return nextWord;
			}

			void setNextWord(ChainedHashWord *next) {
				this->nextWord = next;
			}
	};
};

#endif /* _CHAINEDHASHENTRY_ */

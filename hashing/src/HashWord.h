/*
 * HashEntry.h
 *
 *  Created on: 15 Oct 2014
 *      Author: jvullo
 *
 *
 *  This hash entry can be used for open addressing or linear probing.
 *  It has been extended for use in chaining with chainedHashEntry.h.
 *
 *  This class was going to be used for other collision resolutions,
 *  but I ran out of time.
 */

#ifndef _HASHENTRY_
#define _HASHENTRY_

#include <string>

using namespace std;

namespace SpellCheckerProgram
{

	class HashWord
	{

		protected:
			string key;
			string value;
		public:
			HashWord(string value) {
				this->key = value;
				this->value = value;
			}
			string getWordKey() { return key; }
			string getWordValue() { return value; }
			void setWordValue(string value) {this->value = value;}
	};

}
#endif /*_HASHENTRY_*/

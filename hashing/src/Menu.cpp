/*
 * Menu.cpp
 *
 *  Created on: 22 Oct 2014
 *      Author: jvullo
 */


#include <iostream>
#include <string>
#include <string.h>
#include <stdlib.h>

#include "Menu.h"
#include "SpellChecker.h"

using namespace std;

namespace SpellCheckerProgram
{

	Menu::Menu(SpellChecker *newSpellChecker)
	{
		spellChecker = newSpellChecker;
	}

	void Menu::displayMenu()
	{
		cin.clear();
		int choice = 0;

		cout << endl << "-- Spell Checker Program --" << endl;
		cout << "1 = Spell check file" << endl;
		cout << "2 = Spell check word" << endl;
		cout << "3 = Display load factor" << endl;
		cout << "4 = Print hash map" << endl;
		cout << "5 = Print statistics" << endl;
		cout << "6 = Do hop analysis on a word" << endl;
		cout << "7 = Do hop analysis on a file." << endl;
		cout << "8 = Import dictionary" << endl;


		cout << "Please enter an option from above: ";
		do
		{
			cin >> choice;
			switch(choice)
			{
				case 1:
					spellCheckFile();
					displayMenu();
					break;
				case 2:
					spellCheckWord();
					displayMenu();
					break;
				case 3:
					displayLoadFactor();
					displayMenu();
					break;
				case 4:
					printTable();
					displayMenu();
					break;
				case 5:
					printStats();
					displayMenu();
					break;
				case 6:
					getWordHops();
					displayMenu();
					break;
				case 7:
					getFileHops();
					displayMenu();
					break;
				case 8:
					importDictionary();
					displayMenu();
					break;
				case 0:
					break;
				default:
					cout << "Incorrect input" << endl << endl;
					displayMenu();
					break;
			}
		} while(choice != 0);

	}

	void Menu::spellCheckFile()
	{
		string filename;
		cout << "There are 3 files included for testing, enter \"resources/a.txt\", \"resources/b.txt\" or \"resources/c.txt\" " << endl;
		cout << "Please enter the file name you want to spell check" << endl;
		cin >> filename;
		spellChecker->spellcheckFile(filename);
	}

	void Menu::spellCheckWord()
	{
		string word;
		cout << "Please enter the word you want to spell check" << endl;
		cin >> word;
		spellChecker->spellcheckWord(word);
	}

	void Menu::displayLoadFactor()
	{
		spellChecker->calculateLoadFactor();
	}


	/**
	 * Prints the table.
	 */
	void Menu::printStats()
	{
		spellChecker->printStats();
	}

	/**
	 * Prints the table.
	 */
	void Menu::printTable()
	{
		spellChecker->printTable();
	}


	void Menu::getWordHops()
	{
		string word;
		cout << "Please enter the word you want to spell check" << endl;
		cin >> word;
		spellChecker->testNoHopsWord(word);
	}

	void Menu::getFileHops()
	{
		string filename;
		cout << "There are 3 files included for testing, enter \"resources/a.txt\", \"resources/b.txt\" or \"resources/c.txt\" " << endl;
		cout << "Please enter the file name you want to spell check" << endl;
		cin >> filename;
		spellChecker->testNoHopsFile(filename);
	}


	/**
	 * Prints the table.
	 */
	void Menu::importDictionary()
	{
		//spellChecker->deleteDictionary(); - Not implemented.

		string file;

		cout << "Please enter the name of the dictionary file." << endl;
		cin >> file;

		spellChecker->importDictionary(file);
	}

	/**
	 * Simply quits the program.
	 */
	void Menu::quit()
	{
		cout << "Program will now exit \n" << endl;
		exit(EXIT_SUCCESS);
	}

}




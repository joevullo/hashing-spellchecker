/*
 * menu.h
 *
 *  Created on: 22 Oct 2014
 *      Author: jvullo
 */

#ifndef _MENU_
#define _MENU_

#include "SpellChecker.h"

using namespace std;

namespace SpellCheckerProgram
{
	class Menu
		{
			public:
				Menu(SpellChecker*);
				void displayMenu();
			private:
				SpellChecker *spellChecker;
				void spellCheckFile();
				void spellCheckWord();
				void switchCollisionStrategy();
				void displayLoadFactor();
				void printTable();
				void printStats();
				void getWordHops();
				void getFileHops();
				void importDictionary();
				void quit();

		};
}

#endif /* _MENU_*/


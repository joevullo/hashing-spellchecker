/*
 * SpellChecker.cpp
 *
 *  Created on: 22 Oct 2014
 *      Author: jvullo
 */

#include <iostream>
#include <string>
#include <fstream>
#include <vector>

#include "SpellChecker.h"
#include "HashWord.h"
#include "ChainedHashDictionary.h"
#include "ChainedHashWord.h"

namespace SpellCheckerProgram
{
	/**
	 * Creates the hash table and using the string filename parameter
	 * Imports the dictionary.
	 */
	SpellChecker::SpellChecker(string dictionaryFilename)
	{
		// Initialise hash table.
		map = new ChainedHashDictionary();

		// Import the dictionary to the
		importDictionary(dictionaryFilename);
	}

	/**
	 * Reads a file and returns, a array of strings from that file.
	 *
	 * I know that using vectors is extremely wasteful - its just for the point of this excercise.
	 */
	vector<string> SpellChecker::readFile(string file)
	{

		vector<string> strArr;

		cout << "Beginning reading " + file << endl;

		string word;
		ifstream myfile (file.c_str());
		if (myfile.is_open())
		{
			while ( myfile >> word ) {
				strArr.push_back(word);
			}

		    myfile.close();
		}

		if (strArr.empty()) {
			cout << "Error, file was empty." << endl;
		} else {
			cout << "Reading file was successful" << endl;
		}

		return strArr;
	}

	/**
	 * Imports the dictionary into the program.
	 *
	 * Provide a text file in the parameter to read the dictionary.
	 */
	void SpellChecker::importDictionary(string file)
	{

		cout << "Beginning Importing Dictionary" << endl;
		vector<string> strArr = readFile(file);
		for (vector<string>::size_type i = 0; i < strArr.size(); i++) {
			map->putWord(strArr[i]);
		}
		cout << "Importing Dictionary was successful" << endl;
	}


	/**
	 * Checks an individual word to see if it exists in the stored dictionary.
	 */
	void SpellChecker::testNoHopsWord(string word)
	{
		cout << endl << "Looking for word *" << word << "*" << endl;

		int result = map->getWordHops(word);
		if (result == 0) {
			cout << "Sorry, " << word << " was not found." << endl;
		} else {
			cout << "Found " << word << " in " << result << " hops." << endl;
		}
	}

	/**
	 * Checks an individual word to see if it exists in the stored dictionary.
	 */
	void SpellChecker::testNoHopsFile(string file)
	{
		vector<string> strArr = readFile(file);

		int unsuccessfulHops = 0, successfulHops = 0, countResult = 0, totalSearchesUnsec = 0, totalSearchesSuccesful = 0;

		for (vector<string>::size_type i = 0; i < strArr.size(); i++) {
			countResult = 0;
			string result = map->getWord(strArr[i], countResult);
			if (result.empty()) {
				unsuccessfulHops += countResult;
				totalSearchesUnsec++;
			} else {
				successfulHops += countResult;
				totalSearchesSuccesful++;
			}
		}

		cout << "Number of words searched for: " << strArr.size() << endl;
		cout << "Total number of correct words searched for: " << totalSearchesSuccesful << endl;
		cout << "Total number of incorrect words searched for: " << totalSearchesUnsec << endl;

		float a = (float) successfulHops / (float) totalSearchesSuccesful;
		float b = (float) unsuccessfulHops/ (float) totalSearchesSuccesful;
		float total = ((float) (successfulHops + unsuccessfulHops)) / ((float) (strArr.size()));

		cout << "Average hops for successful words (no of successful hops (" << successfulHops << ") / total successful words search) = " << a << endl;
		cout << "Average hops for unsuccessful words (no of unsuccessful hops(" << unsuccessfulHops << ") / total unsuccessful words search) = " << b << endl;
		cout << "Average hops for all words = " << total << endl;
	}


	/**
	 * Checks an individual word to see if it exists in the stored dictionary.
	 */
	void SpellChecker::spellcheckWord(string word)
	{
		cout << endl << "Looking for word *" << word << "*" << endl;

		if (map->getWord(word).empty()) {
			cout << "Sorry, " << word << " was not found." << endl;
		} else {
			cout << "Found " << word << endl;
		}
	}

	/**
	 * Reads a file and checks that file for spelling mistakes.
	 */
	void SpellChecker::spellcheckFile(string file)
	{
		//std::vector<std::string> strArr[] = readFile(file);
		vector<string> strArr = readFile(file);

		cout << "Printing spell check" << endl;
		cout << "*word* means the word is incorrect." << endl;

		for (vector<string>::size_type i = 0; i < strArr.size(); i++) {
			//cout << strArr[i];

			if (i % 7 == 0) {
				cout << endl;
			}

			if (map->getWord(strArr[i]).empty()) {
				//cout << " *" << strArr[i] << "*";
			} else {
				cout << " " << strArr[i];
			}
		}

		cout << endl;

	}

	/**
	 * Saves the last spelling check to the output file.
	 */
	void SpellChecker::saveLastOutput() {

		  ofstream myfile;
		  myfile.open ("resources/output.txt", ios::app);
		  myfile << "Writing this to a file.\n";
		  myfile.close();
	}

	/**
	 * Wrapper which calls a method that prints a list of hash table.
	 */
	void SpellChecker::printTable()
	{
		map->printHashTable();
	}

	/**
	 * Calculates the load factor.
	 */
	void SpellChecker::calculateLoadFactor()
	{
		map->calculateLoadFactor();
	}

	void SpellChecker::deleteDictionary()
	{
		//map->deleteTable();
	}


	/**
	 * Prints the stats to be used for analysis.
	 */
	void SpellChecker::printStats()
	{

		map->calculateLoadFactor();
		map->calculateLargestChain();
		map->calculateNoEmptyEntries();
		//map->printHashTable();
	}


}

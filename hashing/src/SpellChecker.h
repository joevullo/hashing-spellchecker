/*
 * SpellChecker.h
 *
 *  Created on: 15 Oct 2014
 *      Author: jvullo
 */

#include <string>
#include <iostream>
#include <string>
#include <fstream>
#include <vector>


#ifndef _SPELLCHECKER_
#define _SPELLCHECKER_

#include "ChainedHashDictionary.h"

using namespace std;

namespace SpellCheckerProgram
	{

		class SpellChecker
		{

			private:
				vector<string> readFile(string);
				string readStrArr[];
				ChainedHashDictionary *map;

			public:
				SpellChecker(string);
				void importDictionary(string);
				void spellcheckWord(string);
				void spellcheckFile(string);
				void printIncorrectWords(string []);
				void printStats();
				void printTable();
				void deleteDictionary();
				void testNoHopsWord(string);
				void testNoHopsFile(string);
				void calculateLoadFactor();
				void saveLastOutput();
		};

	}
#endif




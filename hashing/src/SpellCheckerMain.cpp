/*
============================================================================
 * Name        : hashing.cpp
 * Author      : jvullo
 * Version     : 1.01
 * Description : A program that implements a spell check using hashing.
 * ============================================================================
 */


#include <iostream>
#include <cstring>
#include <fstream>
#include "SpellChecker.h"
#include "Menu.h"

using namespace std;
using namespace SpellCheckerProgram;


int main() {

	cout << "Start of program." << endl << endl;

	SpellCheckerProgram::SpellChecker *spellcheckerInstance;
	spellcheckerInstance = new SpellChecker("resources/dictionary.txt");
	SpellCheckerProgram::Menu menu(spellcheckerInstance);
	menu.displayMenu();

	return 0;
}

